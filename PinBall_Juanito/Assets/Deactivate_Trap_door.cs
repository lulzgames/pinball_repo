﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deactivate_Trap_door : MonoBehaviour {

    public GameObject Trap_Door;

    void OnTriggerEnter(Collider ball){
        Trap_Door.SetActive(false);
    }
}

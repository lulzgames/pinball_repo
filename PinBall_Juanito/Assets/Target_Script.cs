﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target_Script : MonoBehaviour {

    public GameObject Target_Original_Pos;
    public GameObject Point_Contorll;
    public int value;
    public float negative_move;
    public bool down;
    bool light_on;
    float timer;
	// Use this for initialization
	void Start () {
        timer = 1f;
        down = false;
        light_on = false;
	}

    void Update()
    {
        if(light_on)
        {
            timer -= 1F * Time.deltaTime;
            if(timer <= 0)
            {
                GetComponentInChildren<Light>().intensity = 0;
            }
        }
    }
	
	void OnCollisionEnter(Collision ball)
    {
        transform.Translate(new Vector3(0, -negative_move, 0));
        Point_Contorll.GetComponent<Points_Controller>().points += value;
        //ball.rigidbody.velocity = Vector3.zero;
        down = true;
        Point_Contorll.GetComponent<Points_Controller>().num_target_down += 1;
        GetComponentInChildren<Light>().intensity = 6;
        light_on = true;
    }
}

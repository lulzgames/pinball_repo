﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activate_Trap_Door : MonoBehaviour {
    public GameObject Trap_Door;
	
    void OnTriggerEnter(Collider ball){
        Trap_Door.SetActive(true);
    }
}

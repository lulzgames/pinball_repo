﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseLife_Script : MonoBehaviour {
    public GameObject Puntuation;

   void OnTriggerExit(Collider ball)
   {
        if(ball.GetComponent<Collider>().tag == "ball")
        {
            Puntuation.GetComponent<Points_Controller>().lifes -= 1;
        }
        
   }
}

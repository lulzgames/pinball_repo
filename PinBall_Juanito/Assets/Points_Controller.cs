﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Points_Controller : MonoBehaviour {
    public GameObject trap_door;
    public GameObject Life_count;
    public GameObject[] Targets;
    public GameObject[] ResetTarget_pos;
    public int points;
    public int num_target_down;
    public int num_lifes;
    public int old_life;
    public int lifes;
    bool game_on;

	// Use this for initialization
	void Start () {
        
        game_on = false;
        points = 0;
        num_target_down = 0;
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<TextMesh>().text = points.ToString();
        Life_count.GetComponent<TextMesh>().text = lifes.ToString();
        if(old_life > lifes)
        {
            for (int i = 0; i < Targets.Length; i++)
            {
                Targets[i].transform.position = ResetTarget_pos[i].transform.position;
            }
            num_target_down = 0;
            old_life = lifes;
        }
       if(lifes <= 0)
        {
            trap_door.SetActive(true);
            game_on = false;
        }
	}

    public void StartGame()
    {
        Debug.Log("in");
        if(!game_on)
        {
            lifes = num_lifes;
            old_life = lifes;
            points = 0;
            trap_door.SetActive(false);
            game_on = true;
        }
        
    }

}

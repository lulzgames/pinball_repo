﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FliperScript : MonoBehaviour {
    public GameObject palaR;
    public GameObject palaL;
    public float force;
    bool palaRUp;
    bool palaLUP;
	// Use this for initialization
	void Start () {
        palaLUP = false;
        palaRUp = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(Input.GetKey(KeyCode.M))
        {
            palaR.GetComponent<Rigidbody>().AddTorque(new Vector3(0, 1, 0) * force ,ForceMode.VelocityChange);
        }
        if(Input.GetKey(KeyCode.Z))
        {
            palaL.GetComponent<Rigidbody>().AddTorque(new Vector3(0, 1, 0) * -force , ForceMode.VelocityChange);
        }
        if(!Input.GetKey(KeyCode.M))
        {
            palaR.GetComponent<Rigidbody>().AddTorque(new Vector3(0,1,0) * -force , ForceMode.VelocityChange); 
        }
        if(!(Input.GetKey(KeyCode.Z)))
        {
            palaL.GetComponent<Rigidbody>().AddTorque(new Vector3(0, 1, 0) * force, ForceMode.VelocityChange);
        }
    }
}

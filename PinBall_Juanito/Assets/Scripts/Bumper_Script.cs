﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper_Script : MonoBehaviour {

    public int value;
    public float check_radius;
    public float repulive_force;
    GameObject Puntuation;

	// Use this for initialization
	void Start () {
        Puntuation = GameObject.Find("Puntuation");
	}
	
    
    void OnCollisionEnter(Collision ball){
        if (Physics.CheckSphere(transform.position,check_radius)){
            //ball.rigidbody.AddForce(new Vector3(repulive_force,0,0), ForceMode.Impulse);
            ball.rigidbody.AddExplosionForce(repulive_force,transform.position,check_radius,-1,ForceMode.VelocityChange);
            if(Puntuation.GetComponent<Points_Controller>().num_target_down == 3)
            {Puntuation.GetComponent<Points_Controller>().points += value*3;}
            else { Puntuation.GetComponent<Points_Controller>().points += value; }
            
        }
    }
}

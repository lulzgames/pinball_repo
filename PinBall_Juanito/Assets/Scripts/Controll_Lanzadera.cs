﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controll_Lanzadera : MonoBehaviour {
    Rigidbody rbody;
    RigidbodyConstraints Lanza_Constr_main;
    RigidbodyConstraints Lanza_Constr_second;
    RigidbodyConstraints Lanza_Constr_gir;
    public float origin_x;
    public float end_x;
    public float origin_y;
    public float end_y;
    public float origin_z;
    float vel = 0;
    bool act_t1;
    public float force_const;
    public float force_limit;
    public float force;
    bool can_impulse;
    Transform Original_pos;
    float timer1;
    public GameObject stop_point;
    // Use this for initialization
    void Start () {
        force = 0;
        origin_z = transform.position.z;
        can_impulse = false;
        Lanza_Constr_main = RigidbodyConstraints.FreezePosition;
        Lanza_Constr_second = RigidbodyConstraints.None;
        Lanza_Constr_gir = RigidbodyConstraints.FreezeRotation;
        rbody = this.gameObject.GetComponent<Rigidbody>();
        origin_x = transform.position.x;
        end_x = origin_x + 4;
        origin_y = transform.position.y;
        end_y = origin_y - 1.8f;
        Original_pos = transform;
        act_t1 = false;
        timer1 = 1f;
        rbody.constraints = Lanza_Constr_main | Lanza_Constr_gir;
    }

	void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (force < force_limit)
            {
                force += force_const*Time.deltaTime;
            }
           
        }
    }

	// Update is called once per frame
	void FixedUpdate () {
		if(Input.GetKey(KeyCode.Space))
        {
            if(transform.position != new Vector3(end_x,end_y,transform.position.z))
            {
                
                float y = Mathf.Lerp(transform.position.y, end_y, 1f*Time.deltaTime);
                float x = Mathf.Lerp(transform.position.x, end_x, 1f*Time.deltaTime);
                Vector3 target_pos = new Vector3(x, y, transform.position.z);
                transform.position = target_pos;
            }
            
        }
        if(Input.GetKeyUp(KeyCode.Space))
        {
            can_impulse = true;
            Lanza_Constr_main = RigidbodyConstraints.FreezePositionZ;
            Lanza_Constr_second = RigidbodyConstraints.FreezePositionY;
            rbody.constraints = Lanza_Constr_main |Lanza_Constr_gir;
            if (can_impulse)
            {
              impulse();
            }
            force = 0;

        }

        
        if (transform.position.x < stop_point.transform.position.x)
        {
            rbody.velocity = Vector3.zero;
            
            act_t1 = true;
            Lanza_Constr_main = RigidbodyConstraints.FreezePosition;
            Lanza_Constr_second = RigidbodyConstraints.None;
            rbody.constraints = Lanza_Constr_main | Lanza_Constr_gir;
        }
        if (act_t1)
        {
            timer1 -= 1 * Time.fixedDeltaTime;
            if (timer1 <= 0)
            {
                transform.position = new Vector3(origin_x, origin_y, transform.position.z);
                timer1 = 1f;
                act_t1 = false;
            }
        }
    }

    void impulse()
    {
       
        rbody.AddForce(new Vector3(-origin_x*force,0,0),ForceMode.Impulse);
        can_impulse = false;

    }
}
